#!/sbin/sh
# Vox Remover
# Based on PA GApps Remover
config="/sdcard/.vox-remove";
prop="/tmp/aroma/config.prop";

echo -n -e "" > $config

# AOSP
	if (grep -qiw "item.1.1=1" $prop ); then
		echo -n -e "BootAnimation\n" >> $config
	fi
	if (grep -qiw "item.1.2=1" $prop ); then
		echo -n -e "Browser\n" >> $config
	fi
	if (grep -qiw "item.1.3=1" $prop ); then
		echo -n -e "Development\n" >> $config
	fi
	if (grep -qiw "item.1.4=1" $prop ); then
		echo -n -e "Email\n" >> $config
	fi
	if (grep -qiw "item.1.5=1" $prop ); then
		echo -n -e "Exchange\n" >> $config
	fi
	if (grep -qiw "item.1.6=1" $prop ); then
		echo -n -e "Gallery\n" >> $config
	fi
	if (grep -qiw "item.1.7=1" $prop ); then
		echo -n -e "Launcher\n" >> $config
	fi
	if (grep -qiw "item.1.8=1" $prop ); then
		echo -n -e "MMS\n" >> $config
	fi
	if (grep -qiw "item.1.9=1" $prop ); then
		echo -n -e "Music\n" >> $config
	fi
	if (grep -qiw "item.1.10=1" $prop ); then
		echo -n -e "Pico\n" >> $config
	fi
	if (grep -qiw "item.1.11=1" $prop ); then
		echo -n -e "SoundRecorder\n" >> $config
	fi
	if (grep -qiw "item.1.12=1" $prop ); then
		echo -n -e "Studio\n" >> $config
	fi
	if (grep -qiw "item.1.13=1" $prop ); then
		echo -n -e "VoiceDialer\n" >> $config
	fi
# CyanogenMod
	if (grep -qiw "item.2.1=1" $prop ); then
		echo -n -e "Apollo\n" >> $config
	fi
	if (grep -qiw "item.2.2=1" $prop ); then
		echo -n -e "CMAudio\n" >> $config
	fi
	if (grep -qiw "item.2.3=1" $prop ); then
		echo -n -e "CMFM\n" >> $config
	fi
	if (grep -qiw "item.2.4=1" $prop ); then
		echo -n -e "CMWall\n" >> $config
	fi
	if (grep -qiw "item.2.5=1" $prop ); then
		echo -n -e "DSP\n" >> $config
	fi
	if (grep -qiw "item.2.6=1" $prop ); then
		echo -n -e "Eleven\n" >> $config
	fi
	if (grep -qiw "item.2.7=1" $prop ); then
		echo -n -e "LockClock\n" >> $config
	fi
	if (grep -qiw "item.2.8=1" $prop ); then
		echo -n -e "Term\n" >> $config
	fi
	if (grep -qiw "item.2.9=1" $prop ); then
		echo -n -e "VoicePlus\n" >> $config
	fi
	if (grep -qiw "item.2.10=1" $prop ); then
		echo -n -e "WhisperPush\n" >> $config
	fi
# Wallpapers/DayDreams
	if (grep -qiw "item.3.1=1" $prop ); then
		echo -n -e "BasicDay\n" >> $config
	fi
	if (grep -qiw "item.3.2=1" $prop ); then
		echo -n -e "BasicWall\n" >> $config
	fi
	if (grep -qiw "item.3.3=1" $prop ); then
		echo -n -e "Galaxy4\n" >> $config
	fi
	if (grep -qiw "item.3.4=1" $prop ); then
		echo -n -e "HoloSpiral\n" >> $config
	fi
	if (grep -qiw "item.3.5=1" $prop ); then
		echo -n -e "MagicSmoke\n" >> $config
	fi
	if (grep -qiw "item.3.6=1" $prop ); then
		echo -n -e "NoiseField\n" >> $config
	fi
	if (grep -qiw "item.3.7=1" $prop ); then
		echo -n -e "PhaseBeam\n" >> $config
	fi
	if (grep -qiw "item.3.8=1" $prop ); then
		echo -n -e "PhotoPhase\n" >> $config
	fi
	if (grep -qiw "item.3.9=1" $prop ); then
		echo -n -e "PhotoDay\n" >> $config
	fi
	if (grep -qiw "item.3.10=1" $prop ); then
		echo -n -e "Visualization\n" >> $config
	fi
# Other
	if (grep -qiw "item.4.1=1" $prop ); then
		echo -n -e "EuphoriaWall\n" >> $config
	fi
	if (grep -qiw "item.4.2=1" $prop ); then
		echo -n -e "SlimFM\n" >> $config
	fi
	if (grep -qiw "item.4.3=1" $prop ); then
		echo -n -e "SlimIRC\n" >> $config
	fi
	if (grep -qiw "item.4.4=1" $prop ); then
		echo -n -e "PACGame\n" >> $config
	fi
	if (grep -qiw "item.4.5=1" $prop ); then
		echo -n -e "PACPapers\n" >> $config
	fi
	if (grep -qiw "item.4.6=1" $prop ); then
		echo -n -e "PALightbulb\n" >> $config
	fi
